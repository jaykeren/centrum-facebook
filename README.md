# Centrum Facebook

Centrum facebook build with adonis (nodejs) for server side and vuejs for client side

## Run Server

```
cd server
npm install
cp .env.example .env
node ace migrate
npm run dev
```

## Run Client

```
cd client
npm install
npm run dev
```
