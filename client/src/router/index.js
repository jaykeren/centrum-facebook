import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/connect",
      name: "connect",
      component: () => import("../views/ConnectView.vue"),
    },
    {
      path: "/statistics",
      name: "statistics",
      component: () => import("../views/StatisticsView.vue"),
    },
    {
      path: "/contents",
      name: "contents",
      component: () => import("../views/contentView.vue"),
    },
  ],
});

export default router;
