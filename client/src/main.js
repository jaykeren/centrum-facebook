import { createApp } from "vue";
import BootstrapVue3 from "bootstrap-vue-3";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebookSquare,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

import App from "./App.vue";
import router from "./router";

import "./App.scss";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue-3/dist/bootstrap-vue-3.css";

library.add(faPhone);
library.add(faFacebookSquare);
library.add(faInstagram);

const app = createApp(App);
app.component("font-awesome-icon", FontAwesomeIcon);
app.use(router);
app.use(BootstrapVue3);
app.mount("#app");
